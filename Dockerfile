FROM python:3.11-slim-bookworm
COPY etc /etc/

WORKDIR /usr/src/app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /usr/src/app

RUN mkdir -p /tmp && chmod 777 /tmp
VOLUME /tmp

CMD [ "python", "./bin/zs-smtpd.py" ]